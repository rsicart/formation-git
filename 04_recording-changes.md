# 04. Enregistrer des modifications

[Lien du livre](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

## Bon maintenant il faut commencer travailler

Mais comment fait on ?

Dans le [premier chapitre](./01_git-basics.md) nous avons parlé des 3 états de nos fichiers...

1. commited: les fichiers sont stockés dans la base de données
2. modified: les fichiers sont modifiés localement, mais pas commited dans la db
3. staged: les fichiers sont marqués comme modifiés, pour être inclus dans le prochain commit

On va dire que ces 3 états font parti d'un super état: **tracked**.
Les fichiers dans les état 1, 2 et 3 sont des fichiers que git connait déjà, pour lesquels git suit (track) son état.

Les fichiers que git ne connait pas ce sont les fichiers avec l'état **untracked**.

Voici le diagramme avec la machine d'état fini du cycle de vie d'un fichier dans git:

![The lifecycle of the status of your files.](https://git-scm.com/book/en/v2/images/lifecycle.png "The lifecycle of the status of your files.")

## Voir le statut de notre dépôt

```
$ git status
```

Ou avec notre alias de ouf:

```
$ git st
```

Version plus courte:

```
$ git st -s
```

## Track un fichier inconnu

```
$ git add README.md
```

## Marquer les modifications d'un fichier comme prêtes pour commit aka Staging Area

C'est la même commande:

```
$ git add README.md
```

Maintenant, en plus, on peut l'exécuter par morceaux, le flag *-p* est là pour rester!

```
$ git add -p README.md
```

## Ignorer des fichiers

Pour dire à git d'ignorer certains fichiers, on peut créer un fichier `.gitignore` dans la racine de notre dépôt.

Dans ce fichier, on va écrire une ligne par pattern à ignorer.

Ouvrez le fichier [.gitignore](./.gitignore) de ce dépôt pour un exemple.


## Voir nos modifications

### nos fichiers avec l'état modified

```
$ git diff
```

### nos fichiers avec l'état staged

Ce sont les fichiers dans la Staging Area.

```
$ git diff --staged
```

## Enregistrer nos changements dans la DB git

```
$ git commit
```

Sans message, seulement avec le titre du commit:

```
$ git commit -m "Fix: mon titre de commit"
```

Voici comment Linus Torvalds décrit un [bon message](https://github.com/torvalds/subsurface-for-dirk/blob/a48494d2fbed58c751e9b7e8fbff88582f9b2d02/README#L88) de commit.

Et un site avec les _best practices_ pour [commit like a boss](https://chris.beams.io/posts/git-commit/): "The seven rules of a great Git commit message"

## Effacer des fichiers

Traduction: transformer des fichiers _tracked_ en _untracked_.

2 façons de faire. Voici la *lente*:

```
$ rm README.md
$ git rm README.md
$ git commit -m "Delete file README"
```

Et la rapide:

```
$ git rm README.md
$ git commit -m "Delete file README"
```

## Renommer des fichiers

```
$ git mv toto.txt titi.txt
$ git commit -m "Rename file toto"
```
