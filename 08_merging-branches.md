# 08. Merging branches

[Lien du livre](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)


## Workflow basique

1. On crée notre branche pour faire un fix ou implémenter une nouvelle feature

```
$ git co -b feature/toto-again
```

2. On fait les modifications

```
$ echo "yeah" > yeah.txt
```

3. On les ajoute dans notre branche

```
$ git add -p
$ git commit -m "Yeah updated, super new feature, an example of what NOT to do as commit message"
```

4. Quand tout est OK, on merge notre branche dans master

```
$ git co master
$ git merge feature/toto-again
```


## Merge Fast-Forward ou pas Fast-Forward?

Quand on merge, git peut le faire en mode fast-forward ou non.

Voyons qu'est que la doc dit sur le sujet:

```
   --ff
       When the merge resolves as a fast-forward, only update the branch pointer, without creating
       a merge commit. This is the default behavior.

   --no-ff
       Create a merge commit even when the merge resolves as a fast-forward. This is the default
       behaviour when merging an annotated (and possibly signed) tag.
```

La différence est que `--ff` ne crée pas de commit de merge.

Du coup, `--ff` ou `--no-ff` ???

* Si notre branche a 1 seul commit: `--ff`
* Dans le reste des cas: `--no-ff`


## Conflits

Si quand on merge une branche dans une autre branche nous avons des conflits, il faut:

1. réviser fichiers 1 par 1
2. trouver les en cherchant dans les fichiers la chaine `<<<`)
3. les corriger
4. suivre les instructions que git nous donne pour la suite

En général, git nous donnera toujours des *hints* pour les commandes qu'il faut exécuter par la suite, faites un `git status` pour les voir.

Dans le pire des cas, si on doute, on **annule** le merge:

```
$ git merge --abort
```

On annule les changements et on demande de l'aide, **pair programming** powah!!
