# 06. Undoing things

[Lien du livre](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things)

## Quand on a déjà commit

Aka commit trop tôt.

Nous pouvons faire plus de modifications pour:
* ajouter quelque chose oublié
* corriger des choses déjà inclues dans le dernier commit

```
$ git commit --amend
```

## Un fichier qui est dans la Staging Area

Nous pouvons bouger un fichier qui se trouve dans la Staging Area vers le Working Directory.

Exécutez un `git status` avant et après pour voir les différences.

```
$ git reset HEAD -- README.md
```

## Dé-modifier un fichier modifié

```
$ git checkout -- README.md
```

## Dé-modifier tous les fichiers modifiés

```
$ git checkout .
```

## Rollback / revert

Pour rollback un commit déjà mergé, on peut utiliser revert pour appliquer le diff à l'envers.
Cela va créer un nouveau commit pour. Easy!

```
$ git revert <commit>
```
