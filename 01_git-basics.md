# 01. Git Basics

[Lien du livre](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)

## Git fait des snapshots, pas de diffs

À chaque commit, git fait un snapshot de l'état de notre dépôt.

On peut dire que chaque commit est un backup, à un instant T, de notre dépôt.

## Git est un système de versionning non centralisé

La plupart des opérations sont locales.

Nous n'avons pas besoin de réseau pour travailler dans notre dépôt.

Par contre, si notre dépôt a un remote de configuré, pour pouvoir pull/push on aura besoin de réseau.

## Git contrôle l'intégrité des donnés

Tous les objets de notre dépôt ont un hash SHA1, et git se sert de ce hash pour les référencer dans sa base de données.

## Les 3 états

Les fichiers, localement, peuvent être dans 3 états différents:

1. commited: les fichiers sont stockés dans la base de données
2. modified: les fichiers sont modifiés localement, mais pas commited dans la db
3. staged: les fichiers sont marqués comme modifiés, pour être inclus dans le prochain commit

![Working tree, staging area, and Git directory.](https://git-scm.com/book/en/v2/images/areas.png "Figure 6. Working tree, staging area, and Git directory.")

Note: voir Figure 6 dans le livre.

## Le workflow habituel

1. checkout le projet, pour avoir une copie en local aka "Working Directory"
2. on fait nos modifications dans notre Working Directory
3. on ajoute nos modifications à la Staging Area
4. on commit
... if finished = false: go to 1
5. on push

## Anatomie d'un commit

[The anatomy of a git commit](https://blog.thoughtram.io/git/2014/11/18/the-anatomy-of-a-git-commit.html)

On a vu que git contrôle l'intégrité des donnés en utilisant des hash SHA1.

Chaque objet dans git a un hash et un mapping:
* 1 fichier est stocké dans un blob (binary large object)
* 1 dossier est stocké dans un tree, qui peut pointer vers d'autres objets (blobs ou trees)

```
. (tree)
├── 01_git-basics.md (blob)
└── README.md (blob)
```

[Pour aller plus loin](https://git-scm.com/book/en/v2/Git-Internals-Git-Objects)


La formule, grosso merdo:

```
Commit = message + commiter + commit date + author + author date + tree
```

__Important__: si l'un des ces champs change, le SHA1 du commit change.
