# 11. Tags

[Lien du livre](https://git-scm.com/book/en/v2/Git-Basics-Tagging)


## Qu'est que c'est un tag?

Un tag est une référence ou pointeur vers un commit. WTF ?!?

On peut aussi le définir comme **une branche statique**.


## Voir les tags

```
$ git tag
```

On peut filtrer les tags qui matchent un pattern:

```
$ git tag -l "v1.0.0"
```

## Création de tags

2 types de tags:

* lightweight

```
$ git tag v1.0.1
```

* annotated: on peut leur définir une description

```
$ git tag -a v1.0.2 "Nouvelle release 1.0.2"
```

On peut également tagger un commit plus tard, en spécifiant son hash:

```
$ git tag v1.0.1 12345abc
```


## Partage des tags

Comme les branches, on peut push les tags pour les partager avec la team:

```
$ git push origin v1.0.1
$ git push origin --tags
```
