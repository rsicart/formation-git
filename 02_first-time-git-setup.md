# 02. Configuration initiale

## Où sont les fichiers de configuration

### 1. globale au système (git config --system)

```
/etc/gitconfig
```

### 2. globale au user (git config --global)

```
~/.gitconfig
~/.config/git/config
```

### 3. spécifique au dépôt (git config --local)

```
.git/config
```

Et les priorités des configurations: 3 > 2 > 1

## Identité

Pour avoir les bonnes informations dans nos commits, et savoir qui a fait quoi...

```
$ git config (--system|--global) user.name "John Doe"
$ git config (--system|--global) user.email johndoe@example.com
```

## Éditeur préféré

Configurer son éditeur pour les messages de commit:

```
$ git config (--system|--global) core.editor vim
```

## Voir nos paramètres de configuration

```
git config --list
```

## Alias utiles

[Lien du livre](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases)

```
$ git config --global alias.co checkout
$ git config --global alias.br branch
$ git config --global alias.ci commit
$ git config --global alias.st status
$ git config --global alias.graph "log --graph --oneline --decorate --color"
$ git config --global alias.grall "log --graph --oneline --decorate --color --all"
```
