# 07. Branches

[Lien du livre](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)

## Remembah

Comme on l'a vu dans le premier chapitre, git sauvegarde des snapshots de l'état de notre dépôt chaque fois qu'on commit.
Nous avons vu également l'anatomie d'un commit.

![Commits and snapshots](https://git-scm.com/book/en/v2/images/commits-and-parents.png)


## Qu'est que c'est une branche?

Une branche est une référence ou pointeur vers un commit.

Chaque fois qu'on ajoute un commit à une branche, on mets à jour cette référence.


## Et HEAD ?

HEAD est une référence ou pointeur à la branche courante.


## Créer une branche

```
$ git branch staging
```

Et pour y travailler:

```
$ git checkout staging
```

Et la commande à utiliser, _all-in-one_:

```
$ git checkout -b feature/toto-is-back
```

![master, staging et HEAD](https://git-scm.com/book/en/v2/images/advance-testing.png)


## Changer d'une branche à une autre

```
$ git checkout master
```

Et pour revenir à la branche précédente:

```
$ git checkout -
```


## Effacer une branche

```
$ git branch -d staging
```

## N'oubliez pas nos alias pour le suivi des branches!

```
$ git graph
$ git grall
```
