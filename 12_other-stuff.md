# 12. Autres outils

## Recherche

[Lien dans le livre](https://git-scm.com/book/en/v2/Git-Tools-Debugging-with-Gi://git-scm.com/book/en/v2/Git-Tools-Searching)

```
$ git grep
```

## Cherry picking

Permets de copier un commit ou plusieurs dans notre branche.

```
$ git cherry-pick <commit>
$ git cherry-pick <commit>..<commit>
```


## Reflog

Le garbage collector de git passe de temps en temps et efface les commits sans références:

* pas de branche
* pas de tags


Entre passage et passage, avec un peu de chance, on va pouvoir trouver nos commits perdus dans notre reflog:

```
$ git reflog
```


## Debug

[Lien dans le livre](https://git-scm.com/book/en/v2/Git-Tools-Debugging-with-Git)

### Qui a fait quoi dans un fichier?

```
$ git blame fichier
```

### Recherche binaire

Pour chercher à quel moment un bug a été introduit dans notre codebase, git intègre un outil de recherche binaire.

```
$ git bisect start
$ git bisect bad
$ git bisect good <commit>
(test)
$ git bisect bad
(test)
$git bisect good
(test)
...
```

#### Exemple pratique

D'abord, clonez ce dépôt.

On va suposer que dans le README.md,  la chaine `git fait un snapshot de tous les fichiers et sous dossiers versionés dans le dépôt` est un bug, et qu'on ne sait pas quand est-ce que ce bug a été introduit dans notre code base.

Pour tester que notre bug est présent ou pas dans notre code, un simple grep de cette chaine va nous l'indiquer:

```
$ git grep "git fait un snapshot de tous les fichiers et sous dossiers versionés dans le dépôt"
```

Avec tous ces elements, on peut commencer à utiliser bisect :)

```
$ git bisect start
Already on 'master'
Your branch is up-to-date with 'origin/master'.
$ git bisect bad
$ git grall
$ git bisect good da50819
Bisecting: 6 revisions left to test after this (roughly 3 steps)
[ce1c9849f2453e14ab4044e9c20b4cdd201570a3] Fix: chapitre 1, explication de snapshot
$ git grep "git fait un snapshot de tous les fichiers et sous dossiers versionés dans le dépôt"
$ git bisect bad
Bisecting: 2 revisions left to test after this (roughly 2 steps)
[fb13d581180e0e83680e9f9a852455e0fda1284b] Merge branch 'rsi/recording-changes-to-repo'
$ git grep "git fait un snapshot de tous les fichiers et sous dossiers versionés dans le dépôt"
$ git bisect good
Bisecting: 0 revisions left to test after this (roughly 1 step)
[892fb21d4b5c4c5f57222dad34dbfdedb9c789d6] Chapitre 6
$ git grep "git fait un snapshot de tous les fichiers et sous dossiers versionés dans le dépôt"
$ git bisect good
ce1c9849f2453e14ab4044e9c20b4cdd201570a3 is the first bad commit
commit ce1c9849f2453e14ab4044e9c20b4cdd201570a3
Author: Roger Sicart <roger.sicart@oxalide.com>
Date:   Mon Jul 2 11:38:55 2018 +0200

    Fix: chapitre 1, explication de snapshot

:100644 100644 69fc8005f0e06126dfc6a6c775bfb312173216ea 6289995a15cae8b573dff2075073d2e9abd7c4eb M      01_git-basics.md
```


On peut recommencer:

```
$ git bisect reset
```

Pour l'automatiser, on peut se faire un petit script/commande de test, et l'automatiser:

```
$ git bisect start <bad commit> <good commit>
$ git bisect run test-error.sh
```

L'exemple de tout à l'heure, en mode devops:

```
$ git bisect start HEAD da50819
$ git bisect run grep -Hnr "git fait un snapshot de tous les fichiers et sous dossiers versionés dans le dépôt"
```
