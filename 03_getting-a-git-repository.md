# 03. Obtenir un dépôt

[Lien du livre](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)

## Initialiser un dépôt dans un dossier existant

```
$ git init
```

## Cloner un dépôt existant

```
$ git clone <url>
```

Examples d'URL:

* http/https
   * https://github.com/libgit2/libgit2
* git
   * git@oxalide.factory.git-01.adm:rsicart/formation-git.git
* local
   * file:///home/rsicart/repositories/formation-git
