# 10. Rebase branches

[Lien du livre](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)


## Qu'est que c'est une rebase une branche?

C'est changer le parent ou parents d'une branche.


## À quoi ça sert?

Plusieurs objectifs:

* réécrire l'historique plus proprement: reword, squash, fixup ou delete de commits
* pouvoir travailler avec notre branche à jour, sans conflits


## Rebase ma branche

```
$ git co fix-ma-branche
$ git rebase master
```

Nous pouvons faire un rebase interactif, qui nous permets plusieurs actions sur nos commits:

* modifier l'ordre
* les regrouper
* en effacer certains
* réécrire le message
*  etc.

```
$ git rebase -i master
```

## Annuler le rebase

Si après un rebase nous avons des conflits, parfois le plus simple est de recommencer.

On annule avec le même flag que pour annuler un merge:

```
$ git rebase --abort
```

## Rebase est une arme très puissante

En effet, avec rebase on réécrit notre historique, cela veut dire que nous pouvons perdre des commits.

Si à cela on y ajoute le `git push --force`, on peut avoir des soucis. Il faut faire très attention....

**Important**: il ne faut pas rebase des commits qui ont été déjà push!
