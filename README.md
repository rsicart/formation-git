# Formation git

![Git logo](https://git-scm.com/images/logo@2x.png "To git like a baws!")

## Introduction

On va parcourir rapidement certaines pages du livre Pro Git, disponible en ligne sur [ici](https://git-scm.com/book/en/v2)

Si vous avez travailé avec d'autres systèmes de versioning centralisés précedents à git, vous pouvez les oublier.

01. [Git Basics](./01_git-basics.md)
02. [Configuration initiale](./02_first-time-git-setup.md)
03. [Obtenir un dépôt](./03_getting-a-git-repository.md)
04. [Enregistrer des modifications](./04_recording-changes.md)
05. [Voir l'historique des commits](./05_viewing-the-commit-history.md)
06. [Undoing things](./06_undoing-things.md)
07. [Branches](./07_branches.md)
08. [Merge de branches](./08_merging-branches.md)
09. [Remote branches](./09_remote-branches.md)
10. [Rebase branches](./10_rebase-branches.md)
11. [Tags](./11_tags.md)
10. [Autres](./12_other-stuff.md)
