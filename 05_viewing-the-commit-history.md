# 05. Voir l'historique des commits

[Lien du livre](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)

## Tout le dépôt

```
$ git log
```

## Un fichier spécifique

```
$ git log -- README.md
```

## Avec notre flag préféré pour voir les diffs

```
$ git log -p -- README.md
```

## Voir juste un résumé rapide

```
$ git log --stat
```

## Limiter aussi la sortie

```
$ git log --since=2.weeks
$ git log --since="2018-05-28"
```

## Représentation graphique

Avec les alias que nous avons crée au début :)

Branche courante:

```
$ git graph
```

Toutes les branches:

```
$ git grall
```

## Linux man est votre ami

Le man git est super complet et avec des exemples!


