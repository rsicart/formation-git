# 09. Remote branches

[Lien du livre](https://git-scm.com/book/en/v2/Git-Branching-Remote-Branches)


## Qu'est que c'est une remote branche?

Une remote branche est une référence ou pointeur vers un commit. WTF?!?

```
$ git ls-remote origin
$ git remote show origin
```

Ces références sont des références de nos dépôts distants (remotes), du type `<remote>/<branche>`.


# Remote-tracking branches

Nous pouvons faire que git fasse un mapping entre les branches remotes et les branches locales.

Cela nous permets d'avoir un suivi, de notre branche remote par rapport à notre branche locale:

* est-ce que nous sommes à jour ?

![Remote branches](https://git-scm.com/book/en/v2/images/remote-branches-4.png)

Si on checkout une branche locale avec le même nom qu'une branche remote, git va automatiquement la tracker.

Si on a déjà une branche locale et on veut que git fasse le tracking avec une branche remote:

```
$ git branch -u origin/serverfix
```


## Partager les changements

Pour partager nos changements locaux avec le reste de la team, il faut push:

```
$ git push origin master
```

Ou l'équivalent, "sur origin, push ma branche locale master sur la branche remote master":

```
$ git push origin master:master
```

Under the hood, git exécute cette commande:

```
$ git push refs/heads/master:refs/heads/master
```


# Effacer une branche remote

Pour effacer une branche sur le dépôt distant, on push "rien" sur notre branche remote `ma-branche`:

```
$ git push origin :ma-branche
```


## Récupérer les changements des branches distantes

Pour récupérer les changements:

```
$ git fetch origin
```

Attention: cela va juste mettre à jour les références des branches remotes. Il faut donc, merge ces changements dans notre branche locale du même nom.

Dans notre branche locale toto:

```
$ git merge origin/toto
```

Note: on peut obtenir le même résultat en utilisant `git reset`.

On peut faire le tout avec une commande, seulement si notre branche est tracké:

```
$ git pull toto
```
